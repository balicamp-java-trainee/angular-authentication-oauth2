---?image=assets/img/bg_logo.png
@title[Angular Authentication]

@snap[east span-60 text-white]
## @color[white](Authentication)
@snapend

@snap[south docslink span-50]
[Angular Docs](https://angular.io/)
@snapend

---?image=assets/img/bg_logo.png
@title[Authentication - Roadmap]

@snap[north-east text-white]
ROADMAP
@snapend

@snap[east span-65]
@ul[spaced text-white]
- Restricting Access
- Session-Base Authentication
- Oauth2
@ulend
@snapend

---?code=assets/src/create.login.ts&lang=typescript
@title[Login]

@[1](Create Login component)
@[3-5](- Make directory helper)
@[3-5](- Create auth-token service)
@[7](Create auth.constant.model.ts)
@[9-14](Fill the constant model)

@snap[north-east template-note text-black]
Authentication - Login
@snapend

---?code=assets/src/auth-token.service.ts&lang=typescript
@title[Token Service]

@[11-19](Create some global variable)
@[21](Inject HttpClient & Router on constructor as private parameter)
@[23-34](Create function login with parameter _username_ & _password_)
@[31](Store _access token_ into local storage browser)
@[36-38](Create function logout for remove _access token_ from local storage browser)
@[40-42](Create function isLogin for checking _access token_ is exist or not from local storage browser)
@[44-46](Create function getAccessToken for return _access token_)

@snap[north-east template-note text-black]
Authentication - Token Service | _auth-token.service.ts_
@snapend

---?code=assets/src/login.component.ts&lang=typescript
@title[Login Component Typescript]

@[13-16](Init variable form type as FormGroup)
@[13-16](Add imports: [ ..., FormsModule,ReactiveFormsModule] in _app.module.ts_)
@[18](Inject AuthTokenService on constructor as private parameter)
@[23-27](Create function login)

@snap[north-east template-note text-black]
Authentication - Login Component | _login.component.ts_
@snapend

---?code=assets/src/login.component.html&lang=html
@title[Login Component HTML]

@[1-11]
@[12-23](Create template login html)

@snap[north-east template-note text-black]
Authentication - Login Component | _login.component.html_
@snapend

---?code=assets/src/http.interceptor.ts&lang=typescript
@title[HTTP Interceptor]

@[1-2](Create _http-interceptor.service_ under helper directory)
@[16](Implement HttpInterceptor from _'@angular/common/http'_)
@[18](Inject AuthTokenService & Router as private parameter on constructor)
@[43](Implement intercept)
@[43-54](Fill logic business on function _intercept_)
@[20](Create function addAccessToken)
@[21-29](Logic for enctype _multipart/form-data_ used by upload)
@[29-40](Generic header normalized)

@snap[north-east template-note text-black]
Authentication - HTTP Interceptor | _http-interceptor.service.ts_
@snapend

---?color=lavender
@title[HTTP Interceptor Provider]

```typescript
providers: [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: HttpInterceptorService,
    multi: true
  }
]
```

@[1-7](Modify providers on _app.module.ts_)

@snap[north-east template-note text-black]
Authentication - HTTP Interceptor Provider | _app.module.ts_
@snapend

---?code=assets/src/app.routing.login.ts&lang=typescript
@title[Routing With Login]

@[2-10](Modify home path routing)
@[2-10](Try _ng serve_ and access from your browser)

@snap[north-east template-note text-black]
Authentication - Routing with login | _app-routing.modul.ts_
@snapend

---?code=assets/src/guard.service.ts&lang=typescript
@title[Guard Service]

@[1-2](Create auth-guard service under src/app/helper)
@[12](Implement CanActivate from _'@angular/router'_)
@[14](Inject Router & AuthTokenService as private parameter on constructor)
@[16-23](Impement canActivate function and fill logic )

@snap[north-east template-note text-black]
Authentication - Guard Service | _auth-guard.service.ts_
@snapend

---?color=lavender
@title[Guard Routing]

```typescript
{
  path: 'product',
  component: ProductListComponent,
  data: { title: 'Data Product' },
  canActivate: [AuthGuardService]
},
{
  path: 'product-detail/:id',
  component: ProductDetailComponent,
  data: { title: 'Data Detail' },
  canActivate: [AuthGuardService]
},
{
  path: 'product-create',
  component: CreateProductComponent,
  data: { title: 'Add New Product' },
  canActivate: [AuthGuardService]
},
{
  path: 'product-edit/:id',
  component: CreateProductComponent,
  data: { title: 'Edit Product' },
  canActivate: [AuthGuardService]
},
{
  path: 'upload-file',
  component: UploadFileComponent,
  data: { title: 'Upload File' },
  canActivate: [AuthGuardService]
}
```

@[5,11,17,23,29](Add canActivate:[AuthGuardService] each routing component)

@snap[north-east template-note text-black]
Authentication - Guard on Router | _app-routing.module.ts_
@snapend

---?image=assets/img/bg_logo.png
@title[THANKYOU]

@snap[east span-50 text-white]
## @color[white](THANK'S)
@snapend
