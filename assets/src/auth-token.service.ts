import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthConstantModel } from './auth.constant.model';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthTokenService {

  private authConstant: AuthConstantModel = new AuthConstantModel();
  private headers = {
    headers: new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded",
      "Authorization": "Basic " + btoa(this.authConstant.client_id + ":" +
        this.authConstant.client_secret),
      "Accept": "*/*"
    })
  };

  constructor(private http: HttpClient, private router: Router) { }

  login(username, password) {
    const body = new URLSearchParams();
    body.append("client_id", this.authConstant.client_id);
    body.append("grant_type", this.authConstant.grant_type);
    body.append("username", username);
    body.append("password", password);
    this.http.post(this.authConstant.url, body.toString(), this.headers)
      .subscribe((res: Response) => {
        localStorage.setItem("access_token", res["access_token"]);
        this.router.navigate(['/product']);
      })
  }

  logout() {
    localStorage.removeItem("access_token");
  }

  isLogin() {
    return localStorage.getItem("access_token") ? true : false;
  }

  getAccessToken(): string {
    return (this.isLogin) ? localStorage.getItem("access_token") : null;
  }
}
