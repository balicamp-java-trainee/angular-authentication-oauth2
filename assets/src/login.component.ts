import { AuthTokenService } from './../helper/auth-token.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    username: new FormControl(),
    password: new FormControl()
  })

  constructor(private authService: AuthTokenService) { }

  ngOnInit() {
  }

  login(){
    this.authService.login(
      this.form.get('username').value,
      this.form.get('password').value);
  }
}
