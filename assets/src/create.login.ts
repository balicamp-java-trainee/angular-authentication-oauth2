ng g c login --spec=false

mkdir helper
cd src/app/helper
ng g s auth-token --spec=false

touch auth.constant.model.ts

export class AuthConstantModel {
    url:string = "http://localhost:8281/uaa/oauth/token";
    client_id:string = "adminapp";
    client_secret:string = "password";
    grant_type:string = "password";
}
