const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: LoginComponent
  },
  {
    path: 'product',
    component: ProductListComponent,
    data: { title: 'Data Product' }
  },
  {
    path: 'product-detail/:id',
    component: ProductDetailComponent,
    data: { title: 'Data Detail' }
  },
  {
    path: 'product-create',
    component: CreateProductComponent,
    data: { title: 'Add New Product' }
  },
  {
    path: 'product-edit/:id',
    component: CreateProductComponent,
    data: { title: 'Edit Product' }
  },
  {
    path: 'upload-file',
    component: UploadFileComponent,
    data: { title: 'Upload File' }]
  }
];
