cd src/app/helper
ng g s http-interceptor --spec=false



import { AuthTokenService } from './auth-token.service';
import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent }
  from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor {

constructor(private authService: AuthTokenService, private router: Router) { }

addAccessToken(req: HttpRequest<any>): HttpRequest<any> {
  if (req.headers.get('enctype') != null
    && req.headers.get('enctype') === 'multipart/form-data') {
    req = req.clone({
      setHeaders: {
        "Authorization": 'Bearer ' + this.authService.getAccessToken()
      }
    });
    return req;
  } else {
    var content_type = (req.headers.get('Content-type') != null
      ? req.headers.get('Content-type')
      : 'application/json');
    req = req.clone({
      setHeaders: {
        'Content-type': content_type,
        "Authorization": 'Bearer ' + this.authService.getAccessToken()
      }
    });
    return req;
  }
}

intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
  if (req.url.indexOf('/token') == -1) {
    if(this.authService.getAccessToken() != null){
      return next.handle(this.addAccessToken(req));
    }else{
      this.authService.logout();
      this.router.navigate(['/']);
    }
  } else {
    return next.handle(req);
  }
}
}
